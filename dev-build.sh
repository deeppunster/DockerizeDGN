#!/bin/bash

# Purpose: run docker-compose build for dev

docker-compose build 2>&1 | tee ./log/dev-build.log

# EOF

