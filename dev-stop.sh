#!/bin/bash

# Purpose: stop currently running docker containers for dev

docker-compose down 2>&1 | tee ./log/dev-stop.log

# EOF

