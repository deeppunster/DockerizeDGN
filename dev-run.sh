#!/bin/bash

# Purpose: Run the previously built docker conntainer(s) for dev

docker-compose up -d 2>&1 | tee ./log/dev-run.log

# EOF

